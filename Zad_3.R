rm(list= ls())

library(MASS)
library(class)

# Funkcja do generowania obserwacji
draw.data.gauss <- function(S1, S2, m1, m2, n1, n2) {
  
  X1 <- mvrnorm(n1, m1, S1)
  X2 <- mvrnorm(n2, m2, S2)
  
  X1 <- data.frame(X1); colnames(X1) <- c("x", "y")
  X2 <- data.frame(X2); colnames(X2) <- c("x", "y")
  
  X1$class <- 1; X2$class <- 2
  
  data <- rbind(X1, X2); data$class <- factor(data$class)
  
  return(data)
}

# Parametry danych z rozkladu Gaussa
S1 <- matrix(c(4, 2, 2, 4), 2, 2)
S2 <- matrix(c(4, 2, 2, 2), 2, 2)

m1 <- c(-1, -1)
m2 <- c(2, 2)

n1 <- 30
n2 <- 20

# Generowanie obserwacji
data <- draw.data.gauss(S1, S2, m1, m2, n1, n2)

k.nn <- 1:21
# Funkcja knn
class.knn <- as.data.frame(sapply(k.nn, function(k) knn(data[,1:2], data[,1:2], data$class, k)))

# Funkcja do macierzy pomy�ek
CM.large <- function(org.class, pred.class) {
  
  CM <- table(org.class, pred.class)
  
  # Skuteczno�� klasyfikatora
  ACC <- sum(diag(CM)) / sum(CM)
  
  # Warto�ci true positive i true negative
  # zak�adamy, �e klasa "2" jest "pozytywna"
  TP <- CM[2,2]
  TN <- CM[1,1]
  
  sums <- apply(CM, 1, sum)
  
  TPR <- TP / sums[2]
  FPR <- 1 - TN / sums[1]
  
  return(c(ACC = round(ACC,4), TP = TP, TN = TN, TPR = round(TPR, 4), FPR = round(FPR, 4), row.names = NULL))
}

data.knn.res <- sapply(k.nn, function(k) CM.large(data$class, class.knn[,k]))

# Rysowanie punkt�w
plot.data <- function(data) {
  
  cols <- c("blue", "orange")
  
  plot(data[,1:2], col = cols[data$class], cex = 2)
  text(data[,1:2], labels = 1:nrow(data), cex = 0.6)
  
}

# Rozpinanie siatki
xp <- seq(-10, 10, 0.1)
yp <- seq(-10, 10, 0.1)

gr <- expand.grid(x = xp, y = yp)

# Klasyfikacja na siatce za pomoc� funkcji knn
k1 <- knn(data[,1:2], gr, data$class, 1)

# Wykre�lanie punkt�w
plot.data(data)

# Granica klasyfikacyjna za pomoc� funkcji knn
contour(xp, yp, matrix(k1 == "1", length(xp)), add = T, levels = 0.5, col = "orange", lty = 2, lwd = 2)

# Skuteczno�� klasyfikatora knn w funkcji liczby najbli�szych s�siad�w
plot(k.nn, data.knn.res[1,], main = "Wykres ACC od knn", ylab = "ACC")
plot(k.nn, data.knn.res[2,], main = "Wykres TP od knn", ylab = "TP") # TP od knn
plot(k.nn, data.knn.res[3,], main = "Wykres TN od knn", ylab = "TN") # TN od knn

# Losowanie zbioru testowego
v.test1 <- sample(which(data$class == 1), 10, replace = TRUE) # zwraca nr-y wierszy dla data
v.test2 <- sample(which(data$class == 2), 5, replace = TRUE)

# Zbi�r testowy
df.test <- data.frame(rbind(data[v.test1,],data[v.test2,]))

# Funkcja knn
class.knn <- as.data.frame(sapply(k.nn, function(k) knn(data[,1:2], df.test[,1:2], data$class, k)))

data.knn.res <- sapply(k.nn, function(k) CM.large(class.knn[,k], df.test$class))

# Wykresy dla losowego zbioru testowego
plot(k.nn, data.knn.res[1,], main = "Wykres ACC od knn - losowy zbi�r testowy", ylab = "ACC") #skuteczno�� od knn
plot(k.nn, data.knn.res[2,], main = "Wykres TP od knn - losowy zbi�r testowy", ylab = "TP") # TP od knn
plot(k.nn, data.knn.res[3,], main = "Wykres TN od knn - losowy zbi�r testowy", ylab = "TN") # TN od knn